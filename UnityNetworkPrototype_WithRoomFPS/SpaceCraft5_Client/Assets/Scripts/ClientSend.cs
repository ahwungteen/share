﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClientSend : MonoBehaviour
{
    #region Send Function
    private static void SendTCPData(Packet _packet)
    {
        _packet.WriteLength();
        Client.instance.tcp.SendData(_packet);
    }

    private static void SendUDPData(Packet _packet)
    {
        _packet.WriteLength();
        Client.instance.udp.SendData(_packet);
    }
    #endregion

    #region Connection
    public static void WelcomeReceived()
    {
        using (Packet packet = new Packet((int)ClientPackets.welcomeReceived))
        {
            packet.Write(Client.instance.myId);

            SendTCPData(packet);
        }
    }
    #endregion

    #region Room
    public static void CreateRoom()
    {
        using (Packet packet = new Packet((int)ClientPackets.createRoom))
        {
            packet.Write(UIManager.instance.usernameField.text);
            packet.Write(UIManager.instance.roomnameField.text);

            SendTCPData(packet);
        }
    }

    public static void JoinRoom()
    {
        using (Packet packet = new Packet((int)ClientPackets.joinRoom))
        {
            packet.Write(UIManager.instance.usernameField.text);
            packet.Write(UIManager.instance.roomnameField.text);

            SendTCPData(packet);
        }
    }

    public static void LeaveRoom()
    {
        using (Packet packet = new Packet((int)ClientPackets.leaveRoom))
        {
            SendTCPData(packet);
        }
    }

    public static void ChangeRoomSetting(int addLevel)
    {
        using (Packet packet = new Packet((int)ClientPackets.changeRoomSetting))
        {
            packet.Write(addLevel);

            SendTCPData(packet);
        }
    }

    public static void LoadLevel()
    {
        using (Packet packet = new Packet((int)ClientPackets.loadLevel))
        {
            SendTCPData(packet);
        }
    }

    public static void GetLevelInfo()
    {
        using (Packet packet = new Packet((int)ClientPackets.getLevelInfo))
        {
            SendTCPData(packet);
        }
    }
    #endregion

    #region Player
    public static void PlayerMovementInput(float[] input)
    {
        using (Packet packet = new Packet((int)ClientPackets.playerMovementInput))
        {
            packet.Write(input.Length);
            foreach (float i in input)
            {
                packet.Write(i);
            }

            SendUDPData(packet);
        }
    }

    public static void PlayerShoot()
    {
        using (Packet packet = new Packet((int)ClientPackets.playerShoot))
        {
            SendTCPData(packet);
        }
    }

    public static void PlayerSwitchWeapon(bool nextWeapon)
    {
        using (Packet packet = new Packet((int)ClientPackets.playerSwitchWeapon))
        {
            packet.Write(nextWeapon);

            SendTCPData(packet);
        }
    }
    #endregion

    #region Obj
    public static void GetObjPosition(string type, int id)
    {
        using (Packet packet = new Packet((int)ClientPackets.getObjPosition))
        {
            packet.Write(type);
            packet.Write(id);

            SendTCPData(packet);
        }
    }
    #endregion
}
