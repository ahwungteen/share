﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ServerObj : MonoBehaviour
{
    public string type { get { return objType; } }
    public int id { get { return objId; } }

    string objType;
    public int objId;

    public bool move = true;
    public GameObject GFX;

    bool destroyed;

    [HideInInspector] public bool serverUpdatePosition;
    [HideInInspector] public Vector3 serverPosition;
    [HideInInspector] public Quaternion serverRotation;

    public void Initialize(string _type, int _id, Quaternion rotation ,Vector3 scale)
    {
        objType = _type;
        objId = _id;
        GFX.transform.rotation = rotation;
        transform.localScale = scale;
    }

    public void ServerMove(Vector3 position, Quaternion rotation)
    {
        serverPosition = position;
        serverRotation = rotation;
        serverUpdatePosition = true;
    }

    void Update()
    {
        if (!move)
        {
            return;
        }

        ClientSend.GetObjPosition(type, id);

        if (serverUpdatePosition)
        {
            transform.position = serverPosition;
            GFX.transform.rotation = serverRotation;
            serverUpdatePosition = false;
        }
    }

    public void DestroyObj()
    {
        if (destroyed)
            return;

        switch (type)
        {
            case "Field":
                if (!GameManager.fields.ContainsKey(id))
                    return;
                Destroy(GameManager.fields[id].gameObject);
                GameManager.fields.Remove(id);
                break;
            case "Player":
                if (!GameManager.players.ContainsKey(id))
                    return;
                Destroy(GameManager.players[id].gameObject);
                GameManager.players.Remove(id);
                break;
            case "Projectile":
                if (!GameManager.projectiles.ContainsKey(id))
                    return;
                Destroy(GameManager.projectiles[id].gameObject);
                GameManager.projectiles.Remove(id);
                break;
            case "Enemy":
                if (!GameManager.enemys.ContainsKey(id))
                    return;
                Destroy(GameManager.enemys[id].gameObject);
                GameManager.enemys.Remove(id);
                break;
            case "Item":
                if (!GameManager.items.ContainsKey(id))
                    return;
                Destroy(GameManager.items[id].gameObject);
                GameManager.items.Remove(id);
                break;
            default:
                Debug.Log("type " + type + "dosen't exist");
                break;
        }

        destroyed = true;
    }

}
