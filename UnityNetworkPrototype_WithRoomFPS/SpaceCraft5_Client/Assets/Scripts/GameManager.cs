﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public static Dictionary<int, ServerObj> fields = new Dictionary<int, ServerObj>();
    public static Dictionary<int, ServerObj> players = new Dictionary<int, ServerObj>();
    public static Dictionary<int, ServerObj> projectiles = new Dictionary<int, ServerObj>();
    public static Dictionary<int, ServerObj> enemys = new Dictionary<int, ServerObj>();
    public static Dictionary<int, ServerObj> items = new Dictionary<int, ServerObj>();

    public GameObject[] fieldPrefabArr;
    public GameObject[] playerPrefabArr;
    public GameObject[] projectileArr;
    public GameObject[] enemyArr;
    public GameObject[] itemArr;

    public GameObject[] explosionArr;

    public GameObject errorMsgPrefab;
    public GameObject inLevelMsgPrefab;
    public GameObject RespawnPrefab;

    public bool isConnected;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this);
        }
        else if (instance != this)
        {
            Debug.Log("Instance already exists, destroying object!");
            Destroy(this);
        }
    }

    public void LoadLevel(int level)
    {
        fields = new Dictionary<int, ServerObj>();
        players = new Dictionary<int, ServerObj>();
        projectiles = new Dictionary<int, ServerObj>();
        enemys = new Dictionary<int, ServerObj>();

        SceneManager.LoadScene(level);
    }

    public void BackMenu()
    {
        fields = new Dictionary<int, ServerObj>();
        players = new Dictionary<int, ServerObj>();
        projectiles = new Dictionary<int, ServerObj>();
        enemys = new Dictionary<int, ServerObj>();

        SceneManager.LoadScene(0);
        ClientSend.LeaveRoom();
    }

    public void ErrorMsg(string msg)
    {
        GameObject obj = Instantiate(errorMsgPrefab, Vector3.zero, Quaternion.identity);
        obj.transform.GetChild(2).GetComponent<Text>().text = msg;
        obj.transform.GetChild(3).GetComponent<Button>().onClick.AddListener(BackMenu);
    }

    public void InLevelMsg(string msg)
    {
        GameObject obj = Instantiate(inLevelMsgPrefab, Vector3.zero, Quaternion.identity);
        obj.transform.GetChild(0).GetChild(1).GetComponent<Text>().text = msg;
    }

    public void SpawnExplosion(int type, Vector3 position, Vector3 scale)
    {
        GameObject obj;

        switch (type)
        {
            case 0:
                obj = Instantiate(explosionArr[0], position, Quaternion.identity);
                break;
            case 1:
                obj = Instantiate(explosionArr[0], position, Quaternion.identity);
                break;
            case 2:
                obj = Instantiate(explosionArr[0], position, Quaternion.identity);
                break;
            case 3:
                obj = Instantiate(explosionArr[0], position, Quaternion.identity);
                break;
            case 4:
                obj = Instantiate(explosionArr[1], position, Quaternion.identity);
                break;
            case 5:
                obj = Instantiate(explosionArr[1], position, Quaternion.identity);
                break;
            default:
                obj = Instantiate(explosionArr[0], position, Quaternion.identity);
                break;
        }
        obj.transform.localScale = scale;
        Destroy(obj, 1);
    }

    ServerObj SpawnPlayer(int id, Vector3 position)
    {
        ServerObj player;
        int index = 0;

        if (id == Client.instance.myId)
        {
            index = 1;
        }

        player = Instantiate(playerPrefabArr[index], position, Quaternion.identity).GetComponent<ServerObj>();
        players.Add(id, player);

        return player;
    }

    public void SpawnObj(string type, int id, int index, Vector3 position, Quaternion rotation, Vector3 scale)
    {
        ServerObj obj;

        switch (type)
        {
            case "Field":
                obj = Instantiate(fieldPrefabArr[index], position, Quaternion.identity).GetComponent<ServerObj>();
                fields.Add(id, obj);
                break;
            case "Player":
                obj = SpawnPlayer(id, position);
                break;
            case "Projectile":
                obj = Instantiate(projectileArr[index], position, Quaternion.identity).GetComponent<ServerObj>();
                projectiles.Add(id, obj);
                break;
            case "Enemy":
                obj = Instantiate(enemyArr[index], position, Quaternion.identity).GetComponent<ServerObj>();
                enemys.Add(id, obj);
                break;
            case "Item":
                obj = Instantiate(itemArr[index], position, Quaternion.identity).GetComponent<ServerObj>();
                items.Add(id, obj);
                break;
            default:
                Debug.Log("Missing Type " + type);
                return;
        }

        obj.Initialize(type, id, rotation, scale);

    }

}
