﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LobbyUIManager : MonoBehaviour
{
    public static LobbyUIManager instance;

    public Text levelText;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    public void ChangeNextLevel(int addLevel)
    {
        ClientSend.ChangeRoomSetting(addLevel);
    }

    public void ChangeLevelFormServer(int nextLevel)
    {
        instance.levelText.text = nextLevel.ToString();
    }

    public void LoadLevel()
    {
        ClientSend.LoadLevel();
    }

}

