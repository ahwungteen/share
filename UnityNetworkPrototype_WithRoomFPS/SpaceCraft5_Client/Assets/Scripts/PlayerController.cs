﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public int id;
    public string username;
    public float speed;

    public Text nameText;
    public Text WeaponText;
    public Slider healthbar;
    public GameObject GFX;

    bool respawning;
    float respawAfterTime;
    GameObject respawnCanvas;

    public void Initialize(string _username, float _speed, int _maxHealth)
    {
        username = _username;
        speed = _speed * 0.7f;

        nameText.text = username;
        healthbar.maxValue = _maxHealth;
        SetHealth(_maxHealth);
    }

    void Update()
    {
        if (respawning && respawnCanvas != null)
        {
            respawAfterTime -= Time.deltaTime;
            if(respawAfterTime < 0)
            {
                respawAfterTime = 0;
            }
            respawnCanvas.transform.GetChild(1).GetComponent<Text>().text = "Respawn After " + Mathf.Ceil(respawAfterTime).ToString() + "...";
        }
    }

    public void Move(float[] input)
    {
        if (GetComponent<ServerObj>().serverUpdatePosition)
        {
            return;
        }

        transform.Translate(new Vector2(input[0], input[1]) * speed * Time.deltaTime);
    }

    public void SetHealth(int newHealth)
    {
        healthbar.value = newHealth;
    }

    public void SetWeapon(string name, int bullet)
    {
        if (bullet > 999)
        {
            WeaponText.text = name + " 999+";
            return;
        }
        WeaponText.text = name + " " + bullet;
    }

    public void Die()
    {
        nameText.enabled = false;
        WeaponText.enabled = false;

        foreach (Transform i in GFX.transform)
        {
            i.GetComponent<Renderer>().enabled = false;
        }

        foreach (Transform i in healthbar.transform)
        {
            i.GetComponent<Image>().enabled = false;
        }
    }

    public void StartRespawn(float _respawnAfterTime)
    {
        respawnCanvas = Instantiate(GameManager.instance.RespawnPrefab, Vector3.zero, Quaternion.identity);

        respawAfterTime = _respawnAfterTime;
        respawning = true;
    }

    public void Respawn()
    {
        respawning = false;
        respawAfterTime = 0;
        if (respawnCanvas != null)
            Destroy(respawnCanvas);


        nameText.enabled = true;
        WeaponText.enabled = true;

        foreach (Transform i in GFX.transform)
        {
            i.GetComponent<Renderer>().enabled = true;
        }

        foreach (Transform i in healthbar.transform)
        {
            i.GetComponent<Image>().enabled = true;
        }

    }
}
