﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public static UIManager instance;

    public GameObject startMenu;
    public GameObject joinRoomMenu;
    public InputField ipField;
    public InputField usernameField;
    public InputField roomnameField;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Debug.Log("Instance already exists, destroying object!");
            Destroy(this);
        }
    }

    public void Start()
    {
        if (!GameManager.instance.isConnected)
        {
            startMenu.SetActive(true);
        }else
        {
            joinRoomMenu.SetActive(true);
        }
    }

    public void ConnectToServer()
    {
        if (ipField.text != "")
        {
            Client.instance.ip = ipField.text;
        }
        startMenu.SetActive(false);
        joinRoomMenu.SetActive(true);
        ipField.interactable = false;
        Client.instance.ConnectToServer();
    }

    public void CreateRoom()
    {
        joinRoomMenu.SetActive(false);
        usernameField.interactable = false;
        roomnameField.interactable = false;
        ClientSend.CreateRoom();
    }

    public void JoinRoom()
    {
        joinRoomMenu.SetActive(false);
        usernameField.interactable = false;
        roomnameField.interactable = false;
        ClientSend.JoinRoom();
    }

}
