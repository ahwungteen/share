﻿using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;

public class ClientHandle : MonoBehaviour
{
    #region Connection
    public static void Welcome(Packet packet)
    {
        string msg = packet.ReadString();
        int myId = packet.ReadInt();

        Debug.Log($"Message from server: {msg}");
        Client.instance.myId = myId;
        ClientSend.WelcomeReceived();

        // Now that we have the client's id, connect UDP
        Client.instance.udp.Connect(((IPEndPoint)Client.instance.tcp.socket.Client.LocalEndPoint).Port);
    }

    public static void PlayerDisconnected(Packet packet)
    {
        int id = packet.ReadInt();

        Destroy(GameManager.players[id].gameObject);
        GameManager.players.Remove(id);
    }
    #endregion

    #region Msg
    public static void ErrorMsg(Packet packet)
    {
        string msg = packet.ReadString();

        GameManager.instance.ErrorMsg(msg);
    }

    public static void InLevelMsg(Packet packet)
    {
        string msg = packet.ReadString();

        GameManager.instance.InLevelMsg(msg);
    }
    #endregion

    #region Level
    public static void LoadLevelScene(Packet packet)
    {
        int scene = packet.ReadInt();

        GameManager.instance.LoadLevel(scene);
    }

    public static void UpdateRoomSetting(Packet packet)
    {
        int nextLevel = packet.ReadInt();

        LobbyUIManager.instance.ChangeLevelFormServer(nextLevel);
    }
    #endregion

    #region Obj
    public static void SpawnObj(Packet packet)
    {
        string type = packet.ReadString();
        int id = packet.ReadInt();
        int index = packet.ReadInt();
        Vector3 position = packet.ReadVector3();
        Quaternion rotation = packet.ReadQuaternion();
        Vector3 scale = packet.ReadVector3();

        GameManager.instance.SpawnObj(type, id, index, position, rotation, scale);
    }

    public static void UpdateObjPosition(Packet packet)
    {
        string type = packet.ReadString();
        int id = packet.ReadInt();
        Vector3 position = packet.ReadVector3();
        Quaternion rotation = packet.ReadQuaternion();

        switch (type)
        {
            case "Field":
                if (!GameManager.fields.ContainsKey(id))
                    return;
                GameManager.fields[id].ServerMove(position, rotation);
                break;
            case "Player":
                if (!GameManager.players.ContainsKey(id))
                    return;
                GameManager.players[id].ServerMove(position, rotation);
                break;
            case "Projectile":
                if (!GameManager.projectiles.ContainsKey(id))
                    return;
                GameManager.projectiles[id].ServerMove(position, rotation);
                break;
            case "Enemy":
                if (!GameManager.enemys.ContainsKey(id))
                    return;
                GameManager.enemys[id].ServerMove(position, rotation);
                break;
            case "Item":
                if (!GameManager.items.ContainsKey(id))
                    return;
                GameManager.items[id].ServerMove(position, rotation);
                break;
            default:
                Debug.Log("type " + type + " dosen't exist");
                break;
        }
    }

    public static void DestroyObj(Packet packet)
    {
        string type = packet.ReadString();
        int id = packet.ReadInt();

        switch (type)
        {
            case "Field":
                if (!GameManager.fields.ContainsKey(id))
                    return;
                GameManager.fields[id].DestroyObj();
                break;
            case "Player":
                if (!GameManager.players.ContainsKey(id))
                    return;
                GameManager.players[id].DestroyObj();
                break;
            case "Projectile":
                if (!GameManager.projectiles.ContainsKey(id))
                    return;
                GameManager.projectiles[id].DestroyObj();
                break;
            case "Enemy":
                if (!GameManager.enemys.ContainsKey(id))
                    return;
                GameManager.enemys[id].DestroyObj();
                break;
            case "Item":
                if (!GameManager.items.ContainsKey(id))
                    return;
                GameManager.items[id].DestroyObj();
                break;
            default:
                Debug.Log("type " + type + "dosen't exist");
                break;
        }
    }
    #endregion

    #region Initialize
    public static void InitializePlayer(Packet packet)
    {
        int id = packet.ReadInt();
        string name = packet.ReadString();
        float speed = packet.ReadFloat();
        int maxHealth = packet.ReadInt();

        if (!GameManager.players.ContainsKey(id))
            return;
        GameManager.players[id].GetComponent<PlayerController>().Initialize(name, speed, maxHealth);
    }
    #endregion

    #region Player
    public static void UpdatePlayerHealthUI(Packet packet)
    {
        int id = packet.ReadInt();
        int health = packet.ReadInt();

        if (!GameManager.players.ContainsKey(id))
            return;
        GameManager.players[id].GetComponent<PlayerController>().SetHealth(health);
    }

    public static void UpdatePlayerWeaponUI(Packet packet)
    {
        int id = packet.ReadInt();
        string name = packet.ReadString();
        int bullet = packet.ReadInt();

        if (!GameManager.players.ContainsKey(id))
            return;
        GameManager.players[id].GetComponent<PlayerController>().SetWeapon(name, bullet);
    }

    public static void PlayerDie(Packet packet)
    {
        int id = packet.ReadInt();
        float respawnTime = packet.ReadFloat();

        if (!GameManager.players.ContainsKey(id))
            return;

        GameManager.players[id].GetComponent<PlayerController>().Die();
        if (id == Client.instance.myId)
            GameManager.players[id].GetComponent<PlayerController>().StartRespawn(respawnTime);
    }

    public static void PlayerRespawn(Packet packet)
    {
        int id = packet.ReadInt();

        if (!GameManager.players.ContainsKey(id))
            return;
        GameManager.players[id].GetComponent<PlayerController>().Respawn();
    }
    #endregion

    public static void SpawnExplosion(Packet packet)
    {
        int type = packet.ReadInt();
        Vector3 position = packet.ReadVector3();
        Vector3 scale = packet.ReadVector3();

        GameManager.instance.SpawnExplosion(type, position, scale);
    }
}
