﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelUIManager : MonoBehaviour
{
    public static LevelUIManager instance;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    public void BackMenu()
    {
        GameManager.instance.BackMenu();
    }

    public void PlayerSwitchWeapon(bool nextWeapon)
    {
        FindObjectOfType<PlayerInputManager>().SwitchWeapon(nextWeapon);
    }
}
