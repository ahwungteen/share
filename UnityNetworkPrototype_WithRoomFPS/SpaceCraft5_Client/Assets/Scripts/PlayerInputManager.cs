﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Cinemachine;

public class PlayerInputManager : MonoBehaviour
{
    Joystick joystick;
    [SerializeField] bool localMove;

    void Start()
    {
        joystick = FindObjectOfType<Joystick>();
        FindObjectOfType<CinemachineVirtualCamera>().Follow = transform;
        GameObject.FindGameObjectWithTag("ShootButton").GetComponent<Button>().onClick.AddListener(Shoot);
    }

    void FixedUpdate()
    {
        CheckInput();
    }

    void CheckInput()
    {
        float[] input = new float[2];

        if (joystick.Horizontal > 0.2f || joystick.Horizontal < -0.2f)
        {
            input[0] = joystick.Horizontal;
        }
        if (joystick.Vertical > 0.2f || joystick.Vertical < -0.2f)
        {
            input[1] = joystick.Vertical;
        }

        ClientSend.PlayerMovementInput(input);

        if (localMove)
            GetComponent<PlayerController>().Move(input);
    }

    public void Shoot()
    {
        ClientSend.PlayerShoot();
    }

    public void SwitchWeapon(bool nextWeapon)
    {
        ClientSend.PlayerSwitchWeapon(nextWeapon);
    }
}
