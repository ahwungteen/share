﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
    public int id;

    [HideInInspector] public bool destroyed;

    public void Initialize(int _id)
    {
        id = _id;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("PlayerGFX"))
        {
            float random = Random.Range(0f, 1f);
            Transform player = other.transform.parent;

            if (random < 0.3f)
            {
                player.GetComponent<Player>().Heal(player.GetComponent<Player>().maxHealth);
            }
            else
            {
                player.GetComponent<PlayerWeaponController>().PickUpItem();
            }

            DestroyItem();
        }
    }

    public void DestroyItem()
    {
        if (destroyed)
            return;

        ServerSend.DestroyObj(GetComponent<RoomId>().id, "Item", id);
        Server.rooms[GetComponent<RoomId>().id].itemList.Remove(id);
        Destroy(Server.items[id].gameObject);

        destroyed = true;
    }
}
