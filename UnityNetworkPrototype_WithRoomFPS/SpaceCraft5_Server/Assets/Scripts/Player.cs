﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public int roomId;
    public int id;
    public string username;
    public float speed = 5;

    public int maxHealth = 100;
    public int health;
    public float respawnTime;
    public bool died;

    public Vector2 facingDirection = Vector2.right;

    [HideInInspector] public bool destroyed;

    [Header("Get Component")]
    public Rigidbody2D rb;

    private float[] input;

    public void Initialize(int _id, string _username, int _roomId)
    {
        id = _id;
        username = _username;
        roomId = _roomId;
        health = maxHealth;

        input = new float[2];

        ServerSend.UpdatePlayerHealthUI(roomId, id, health);
    }

    public void SetInput(float[] playerInput)
    {
        input = playerInput;
    }

    void FixedUpdate()
    {
        if (died)
            return;

        if (input.Length != 2)
        {
            Debug.Log("Error___MovementInput " + "Input Length != 2!");
            return;
        }

        float moveX = input[0];
        float moveY = input[1];

        if (moveX >= 1)
        {
            moveX = 1;
        }
        else if (moveX <= -1)
        {
            moveX = -1;
        }
        if (moveY >= 1)
        {
            moveY = 1;
        }
        else if (moveY <= -1)
        {
            moveY = -1;
        }

        rb.position += new Vector2(moveX, moveY) * speed * Time.deltaTime;

        if (moveX == 0 && moveY == 0)
            return;

        facingDirection = new Vector2(moveX, moveY);
        Rotate(moveX, moveY);
    }

    void Rotate(float moveX, float moveY)
    {
        float angle = 0;
        if (moveX == 0 || (moveX < 1 && moveX > 0))
        {
            angle = 90 * moveY;
        }
        else if (moveX > -1 && moveX < 0)
        {
            angle = 180 + 90 * -moveY;
        }

        transform.rotation = Quaternion.Euler(0, 0, angle);
    }

    public void TakeDamage(int damage)
    {
        health -= damage;
        ServerSend.UpdatePlayerHealthUI(roomId, id, health);

        if (health <= 0 && !died)
        {
            Die();

            if (Server.rooms[roomId].level == 0) //check if in lobby
            {
                StartCoroutine(Respawn());
                return;
            }

            foreach (int i in Server.rooms[roomId].playerList)
            {
                if (Server.clients[i].player != null && !Server.clients[i].player.died) //check if anyone not died
                {
                    StartCoroutine(Respawn());
                    return;
                }
            }
            Server.rooms[roomId].GameOver();
        }
    }

    public void Heal(int healAmount)
    {
        health += healAmount;
        ServerSend.UpdatePlayerHealthUI(roomId, id, health);

        if (health > maxHealth)
        {
            health = maxHealth;
        }
    }

    void Die()
    {
        Debug.Log("Player " + id + " Died");
        ServerSend.InLevelMsg(GetComponent<RoomId>().id, Server.clients[id].name + " Died");

        foreach (Transform i in transform)
        {
            if(i.GetComponent<Collider2D>() != null)
            {
                i.GetComponent<Collider2D>().enabled = false;
            }
        }
        died = true;
        ServerSend.PlayerDie(roomId, id, respawnTime);
    }

    IEnumerator Respawn()
    {
        yield return new WaitForSeconds(respawnTime);

        foreach (Transform i in transform)
        {
            if (i.GetComponent<Collider2D>() != null)
            {
                i.GetComponent<Collider2D>().enabled = true;
            }
        }
        died = false;
        health = maxHealth;
        transform.position = new Vector3(0, -2, 0);
        ServerSend.UpdatePlayerHealthUI(roomId, id, health);
        ServerSend.PlayerRespawn(roomId, id);
    }

    public void DestroyPlayer()
    {
        if (destroyed)
            return;

        ServerSend.DestroyObj(roomId, "Player", id);
        Destroy(Server.clients[id].player.gameObject);
        Server.clients[id].player = null;

        destroyed = true;
    }

}