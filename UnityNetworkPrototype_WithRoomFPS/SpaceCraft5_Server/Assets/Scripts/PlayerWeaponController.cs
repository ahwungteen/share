﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerWeaponController : MonoBehaviour
{
    public GameObject[] weaponPrefabArr;
    public int[] weaponUnlockInWave;
    public Transform firePoint;

    [HideInInspector] public List<PlayerWeapon> currentWeaponArr = new List<PlayerWeapon>();
    [HideInInspector] public int currentWeapon;

    public float currentAttackSpeed;
    float attackSpeedCount;
    bool canShoot = true;

    void Awake()
    {
        NewWeapon(0);
        currentWeapon = 0;
        currentAttackSpeed = currentWeaponArr[0].attackSpeed;
        currentWeaponArr[0].infinity = true;
    }

    void Start()
    {
        UpdateWeaponUI();
    }

    void Update()
    {
        if (!canShoot)
        {
            if (attackSpeedCount > currentAttackSpeed)
            {
                canShoot = true;
                attackSpeedCount = 0;
            }
            attackSpeedCount += Time.deltaTime;
        }
    }

    public void UpdateWeaponUI()
    {
        if (currentWeaponArr[currentWeapon].infinity)
        {
            ServerSend.UpdatePlayerWeaponUI(GetComponent<RoomId>().id, GetComponent<Player>().id, currentWeaponArr[currentWeapon].weaponName, 1000);
            return;
        }
        ServerSend.UpdatePlayerWeaponUI(GetComponent<RoomId>().id, GetComponent<Player>().id, currentWeaponArr[currentWeapon].weaponName, currentWeaponArr[currentWeapon].bulletAmount);
    }

    public void Shoot(int roomId, Vector2 direction)
    {
        if (!canShoot)
            return;

        canShoot = false;

        bool haveBullet = currentWeaponArr[currentWeapon].Shoot(roomId, firePoint.position, direction);
        if (!haveBullet)
        {
            Destroy(currentWeaponArr[currentWeapon].gameObject);
            currentWeaponArr.Remove(currentWeaponArr[currentWeapon]);
            SwitchNextWeapon(true);
        }
        UpdateWeaponUI();
    }

    public void SwitchNextWeapon(bool nextWeapon)
    {
        int addWeapon;

        if (nextWeapon)
        {
            addWeapon = 1;
        }
        else
        {
            addWeapon = -1;
        }

        currentWeapon += addWeapon;

        if (currentWeapon > currentWeaponArr.Count - 1)
        {
            currentWeapon = 0;
        }
        if (currentWeapon < 0)
        {
            currentWeapon = currentWeaponArr.Count - 1;
        }

        UpdateWeaponUI();
        currentAttackSpeed = currentWeaponArr[currentWeapon].attackSpeed;
        canShoot = false;
    }

    public void PickUpItem()
    {
        int wave = Server.rooms[GetComponent<RoomId>().id].wave;

        if (currentWeaponArr.Count <= 1) // only have 1 weapon
        {
            UnlockRandomWeapon();
            return;
        }

        for (int i = 0; i < weaponUnlockInWave.Length; i++)
        {
            if (weaponUnlockInWave[i] > wave)
            {
                if (currentWeaponArr.Count < i - 1) // still can unlock
                {
                    float random = Random.Range(-1f, 1f);

                    if (random > 0)
                    {
                        UnlockRandomWeapon();
                    }
                }
                FillRandomWeaponBullet();
                return;
            }
        }

        if (currentWeaponArr.Count < weaponUnlockInWave.Length) // still can unlock
        {
            float random = Random.Range(-1f, 1f);

            if (random > 0)
            {
                UnlockRandomWeapon();
                return;
            }
        }

        FillRandomWeaponBullet();
    }

    void FillRandomWeaponBullet()
    {
        List<int> canFillList = new List<int>();

        for (int i = 0; i < currentWeaponArr.Count; i++)
        {
            if (!currentWeaponArr[i].infinity)
            {
                canFillList.Add(i);
            }
        }

        if (canFillList.Count <= 0)
            return;

        int random = canFillList[Random.Range(0, canFillList.Count)];

        currentWeaponArr[random].FillBullet();
        UpdateWeaponUI();
    }

    void UnlockRandomWeapon()
    {
        List<int> canUnlockArr = new List<int>();

        int wave = Server.rooms[GetComponent<RoomId>().id].wave;

        for (int i = 0; i < weaponUnlockInWave.Length; i++)
        {
            if (weaponUnlockInWave[i] < wave)
            {
                bool haveWeapon = false;
                foreach (PlayerWeapon j in currentWeaponArr) //check whether have weapon
                {
                    if (j.type == i)
                    {
                        haveWeapon = true;
                    }
                }
                if (!haveWeapon)
                {
                    canUnlockArr.Add(i);
                }
            }
        }

        if (canUnlockArr.Count <= 0)
        {
            FillRandomWeaponBullet();
            return;
        }

        int random = Random.Range(0, canUnlockArr.Count);
        NewWeapon(canUnlockArr[random]);
    }

    void NewWeapon(int type)
    {
        PlayerWeapon weapon = Instantiate(weaponPrefabArr[type], Vector3.zero, Quaternion.identity).GetComponent<PlayerWeapon>();
        weapon.Initialize(type);
        weapon.transform.SetParent(transform);
        currentWeaponArr.Add(weapon);

        if (type == 0)
            return;
        ServerSend.InLevelMsg(GetComponent<RoomId>().id, Server.clients[GetComponent<Player>().id].name + " Unlocked " + weapon.weaponName);
    }

    public void PlayerDie()
    {
        for (int i = currentWeaponArr.Count - 1; i >= 0; i--)
        {
            if (i <= 0)
            {
                SwitchNextWeapon(true);
                return;
            }

            Destroy(currentWeaponArr[i].gameObject);
            currentWeaponArr.Remove(currentWeaponArr[i]);
        }
    }
}
