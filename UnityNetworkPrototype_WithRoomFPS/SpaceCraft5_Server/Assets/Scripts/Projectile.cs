﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    [HideInInspector] public int id;
    [HideInInspector] public int type;
    [HideInInspector] public float force;
    [HideInInspector] public Vector2 direction;
    [HideInInspector] public int damage;
    [HideInInspector] public float destroyAfterDistence;
    [HideInInspector] float destroyDistenceCount;

    [Header("Explosion")]
    [HideInInspector] public bool explosion;
    [HideInInspector] public bool destroyByTime;
    [HideInInspector] public float explosionAfterTime;
    [HideInInspector] public float radius;
    [HideInInspector] public float explosionForce;

    public Rigidbody2D rb;

    [HideInInspector] public bool destroyed;

    public void Initialize(int _id, int _type, float _force, Vector2 _direction, int _damage, float _destroyAfterDistence)
    {
        id = _id;
        type = _type;
        force = _force;
        direction = _direction;
        damage = _damage;
        destroyAfterDistence = _destroyAfterDistence;
    }

    public void InitializeExplosion(bool _explosion, bool _destroyByTime, float _explosionAfterTime, float _radius, float _explosionForce)
    {
        explosion = _explosion;
        destroyByTime = _destroyByTime;
        explosionAfterTime = _explosionAfterTime;
        radius = _radius;
        explosionForce = _explosionForce;
    }

    void Start()
    {
        rb.AddForce(direction * force);

        if (destroyByTime && explosion)
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                transform.GetChild(i).GetComponent<Collider2D>().isTrigger = false;
            }
            StartCoroutine(Explosion());
        }
    }

    void Update()
    {
        if (destroyByTime)
            return;

        if (destroyDistenceCount > destroyAfterDistence)
        {
            DestroyProjectile();
            destroyDistenceCount = 0;
        }
        destroyDistenceCount += Time.deltaTime;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (destroyByTime)
            return;

        if (explosion)
        {
            StartCoroutine(Explosion());
            return;
        }

        if (other.transform.parent.CompareTag("Player"))
        {
            other.transform.parent.GetComponent<Player>().TakeDamage(damage);
        }
        if (other.transform.parent.CompareTag("Enemy"))
        {
            other.transform.parent.GetComponent<Enemy>().TakeDamage(damage);
        }

        ServerSend.SpawnExplosion(GetComponent<RoomId>().id, type, transform.position, transform.localScale);
        DestroyProjectile();
    }

    IEnumerator Explosion()
    {
        yield return new WaitForSeconds(explosionAfterTime);

        Vector3 explosionPos = transform.position;
        Collider2D[] colliders = Physics2D.OverlapCircleAll(explosionPos, radius);

        foreach (Collider2D hit in colliders)
        {
            Transform hitObj = hit.gameObject.transform.parent;

            if ((hit.CompareTag("PlayerGFX") || hit.CompareTag("EnemyGFX")) && hitObj.GetComponent<Rigidbody2D>() != null)
            {
                Rigidbody2DExt.AddExplosionForce(hitObj.GetComponent<Rigidbody2D>(), explosionForce, explosionPos, radius);
                switch (hitObj.tag)
                {
                    case "Player":
                        hitObj.GetComponent<Player>().TakeDamage(damage);
                        break;
                    case "Enemy":
                        hitObj.GetComponent<Enemy>().TakeDamage(damage);
                        break;
                }
            }
        }
        ServerSend.SpawnExplosion(GetComponent<RoomId>().id, type, transform.position, new Vector3(radius * 2, radius * 2, 1));
        DestroyProjectile();
    }

    public void DestroyProjectile()
    {
        if (destroyed)
            return;

        ServerSend.DestroyObj(GetComponent<RoomId>().id, "Projectile", id);
        Server.rooms[GetComponent<RoomId>().id].projectileList.Remove(id);
        Destroy(Server.projectiles[id].gameObject);

        destroyed = true; 
    }
}
