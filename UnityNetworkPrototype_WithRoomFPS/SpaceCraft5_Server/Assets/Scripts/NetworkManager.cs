﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkManager : MonoBehaviour
{
    public static NetworkManager instance;

    public GameObject[] FieldPrefabArr;
    public GameObject[] playerPrefabArr;
    public GameObject[] projectileListPrefabArr;
    public GameObject[] enemyPrefabArr;
    public GameObject[] itemPrefabArr;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Debug.Log("Instance already exists, destroying object!");
            Destroy(this);
        }
    }

    private void Start()
    {
        QualitySettings.vSyncCount = 0;

        Server.Start(50, 26950);
    }

    private void OnApplicationQuit()
    {
        Server.Stop();
    }

    public GameObject InstantiateObj(string type, int roomId, Vector3 position, int index)
    {
        GameObject obj = new GameObject();
        Destroy(obj);

        switch (type)
        {
            case "Field":
                obj = Instantiate(FieldPrefabArr[index], position, Quaternion.identity);
                break;
            case "Player":
                obj = Instantiate(playerPrefabArr[index], position, Quaternion.identity);
                break;
            case "Projectile":
                obj = Instantiate(projectileListPrefabArr[index], position, Quaternion.identity);
                break;
            case "Enemy":
                obj = Instantiate(enemyPrefabArr[index], position, Quaternion.identity);
                break;
            case "Item":
                obj = Instantiate(itemPrefabArr[index], position, Quaternion.identity);
                break;
            default:
                Debug.Log("missing instantiate type " + type);
                break;
        }

        obj.GetComponent<RoomId>().id = roomId;
        for (int i = 0; i < obj.transform.childCount; i++)
        {
            if (obj.transform.GetChild(i).GetComponent<RoomId>() != null)
            {
                obj.transform.GetChild(i).GetComponent<RoomId>().id = roomId;
            }
        }

        return obj;
    }
}
