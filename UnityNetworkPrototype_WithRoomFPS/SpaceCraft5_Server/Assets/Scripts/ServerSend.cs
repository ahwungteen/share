﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ServerSend
{
    #region Send Function
    private static void SendTCPData(int _toClient, Packet _packet)
    {
        _packet.WriteLength();
        Server.clients[_toClient].tcp.SendData(_packet);
    }

    private static void SendUDPData(int _toClient, Packet _packet)
    {
        _packet.WriteLength();
        Server.clients[_toClient].udp.SendData(_packet);
    }

    private static void SendTCPDataToAll(Packet _packet)
    {
        _packet.WriteLength();
        for (int i = 1; i <= Server.MaxPlayers; i++)
        {
            Server.clients[i].tcp.SendData(_packet);
        }
    }

    private static void SendTCPDataToAll(int _exceptClient, Packet _packet)
    {
        _packet.WriteLength();
        for (int i = 1; i <= Server.MaxPlayers; i++)
        {
            if (i != _exceptClient)
            {
                Server.clients[i].tcp.SendData(_packet);
            }
        }
    }
    private static void SendTCPDataToAllInRoom(int roomId, Packet _packet)
    {
        if (roomId == 0 || Server.rooms[roomId].playerList.Count <= 0) //client not in room or no client in rooms
            return;

        _packet.WriteLength();

        foreach (int i in Server.rooms[roomId].playerList)
        {
            Server.clients[i].tcp.SendData(_packet);
        }
    }

    private static void SendTCPDataToAllInRoomInLevel(int roomId, Packet _packet)
    {
        if (roomId == 0 || Server.rooms[roomId].playerList.Count <= 0) //client not in room or no client in rooms
            return;

        _packet.WriteLength();

        foreach (int i in Server.rooms[roomId].playerList)
        {
            if (Server.clients[i].inLevel)
            {
                Server.clients[i].tcp.SendData(_packet);
            }
        }
    }

    private static void SendUDPDataToAll(Packet _packet)
    {
        _packet.WriteLength();
        for (int i = 1; i <= Server.MaxPlayers; i++)
        {
            Server.clients[i].udp.SendData(_packet);
        }
    }

    private static void SendUDPDataToAll(int _exceptClient, Packet _packet)
    {
        _packet.WriteLength();
        for (int i = 1; i <= Server.MaxPlayers; i++)
        {
            if (i != _exceptClient)
            {
                Server.clients[i].udp.SendData(_packet);
            }
        }
    }
    private static void SendUDPDataToAllInRoom(int roomId, Packet _packet)
    {
        if (roomId == 0 || Server.rooms[roomId].playerList.Count <= 0) //client not in room or no client in rooms
            return;

        _packet.WriteLength();
        foreach (int i in Server.rooms[roomId].playerList)
        {
            Server.clients[i].udp.SendData(_packet);
        }
    }

    #endregion

    #region Connection
    public static void Welcome(int toClient, string msg)
    {
        using (Packet packet = new Packet((int)ServerPackets.welcome))
        {
            packet.Write(msg);
            packet.Write(toClient);

            SendTCPData(toClient, packet);
        }
    }

    public static void PlayerDisconnected(int playerId, int roomId)
    {
        using (Packet packet = new Packet((int)ServerPackets.playerDisconnected))
        {
            packet.Write(playerId);

            SendTCPDataToAllInRoom(roomId, packet);
        }
    }
    #endregion

    #region Msg
    public static void ErrorMsg(int toClient, string msg)
    {
        using (Packet packet = new Packet((int)ServerPackets.errorMsg))
        {
            packet.Write(msg);

            SendTCPData(toClient, packet);
        }
    }

    public static void InLevelMsg(int roomId, string msg)
    {
        using (Packet packet = new Packet((int)ServerPackets.inLevelMsg))
        {
            packet.Write(msg);

            SendTCPDataToAllInRoomInLevel(roomId, packet);
        }
    }
    #endregion

    #region Level
    public static void LoadLevelScene(int toClient, int scene)
    {
        using (Packet packet = new Packet((int)ServerPackets.loadLevelScene))
        {
            packet.Write(scene);

            SendTCPData(toClient, packet);
        }
    }

    public static void LoadLevelSceneToAllInRoom(int roomId, int level)
    {
        using (Packet packet = new Packet((int)ServerPackets.loadLevelScene))
        {
            packet.Write(level);

            SendTCPDataToAllInRoom(roomId, packet);
        }
    }

    public static void UpdateRoomSetting(int roomId, int nextLevel)
    {
        using (Packet packet = new Packet((int)ServerPackets.updateRoomSetting))
        {
            packet.Write(nextLevel);

            SendTCPDataToAllInRoomInLevel(roomId, packet);
        }
    }
    #endregion

    #region Obj
    public static void SpawnObj(int toClient, string type, int spawnObjId, int index, Vector3 position, Quaternion rotation, Vector3 scale)
    {
        using (Packet packet = new Packet((int)ServerPackets.spawnObj))
        {
            packet.Write(type);
            packet.Write(spawnObjId);
            packet.Write(index);
            packet.Write(position);
            packet.Write(rotation);
            packet.Write(scale);

            SendTCPData(toClient, packet);
        }
    }

    public static void SpawnObjToAllInRoom(int roomId, string type, int spawnObjId, int index, Vector3 position, Quaternion rotation, Vector3 scale)
    {
        using (Packet packet = new Packet((int)ServerPackets.spawnObj))
        {
            packet.Write(type);
            packet.Write(spawnObjId);
            packet.Write(index);
            packet.Write(position);
            packet.Write(rotation);
            packet.Write(scale);

            SendTCPDataToAllInRoomInLevel(roomId, packet);
        }
    }

    public static void UpdateObjPosition(int roomId, string type, int id)
    {
        using (Packet packet = new Packet((int)ServerPackets.updateObjPosition))
        {
            Vector3 position = Vector3.zero;
            Quaternion rotation = Quaternion.identity;

            switch (type)
            {
                case "Player":
                    if (Server.clients[id].player == null || Server.clients[id].player.destroyed)
                        return;
                    position = Server.clients[id].player.transform.position;
                    rotation = Server.clients[id].player.transform.rotation;
                    break;
                case "Projectile":
                    if (!Server.projectiles.ContainsKey(id) || Server.projectiles[id].destroyed)
                        return;
                    position = Server.projectiles[id].transform.position;
                    rotation = Server.projectiles[id].transform.rotation;
                    break;
                case "Enemy":
                    if (!Server.enemys.ContainsKey(id) || Server.enemys[id].destroyed)
                        return;
                    position = Server.enemys[id].transform.position;
                    rotation = Server.enemys[id].transform.rotation;
                    break;
                case "Item":
                    if (!Server.items.ContainsKey(id) || Server.items[id].destroyed)
                        return;
                    position = Server.items[id].transform.position;
                    rotation = Server.items[id].transform.rotation;
                    break;
                default:
                    Debug.Log("Missing Type " + type);
                    return;
            }

            packet.Write(type);
            packet.Write(id);
            packet.Write(position);
            packet.Write(rotation);

            SendTCPDataToAllInRoomInLevel(roomId, packet);
        }
    }

    public static void DestroyObj(int roomId, string type, int id)
    {
        using (Packet packet = new Packet((int)ServerPackets.destroyObj))
        {
            packet.Write(type);
            packet.Write(id);

            SendTCPDataToAllInRoomInLevel(roomId, packet);
        }
    }
    #endregion

    #region Initialize
    public static void InitializePlayer(int toClient, int id, string name, float speed, int maxHealth)
    {
        using (Packet packet = new Packet((int)ServerPackets.initializePlayer))
        {
            packet.Write(id);
            packet.Write(name);
            packet.Write(speed);
            packet.Write(maxHealth);

            SendTCPData(toClient, packet);
        }
    }

    public static void InitializePlayerToAllInRoom(int roomId, int id, string name, float speed, int maxHealth)
    {
        using (Packet packet = new Packet((int)ServerPackets.initializePlayer))
        {
            packet.Write(id);
            packet.Write(name);
            packet.Write(speed);
            packet.Write(maxHealth);

            SendTCPDataToAllInRoomInLevel(roomId, packet);
        }
    }
    #endregion

    #region Player
    public static void UpdatePlayerHealthUI(int roomId, int id, int health)
    {
        using (Packet packet = new Packet((int)ServerPackets.updatePlayerHealthUI))
        {
            packet.Write(id);
            packet.Write(health);

            SendTCPDataToAllInRoomInLevel(roomId, packet);
        }
    }

    public static void UpdatePlayerWeaponUI(int roomId, int id, string name, int bullet)
    {
        using (Packet packet = new Packet((int)ServerPackets.updatePlayerWeaponUI))
        {
            packet.Write(id);
            packet.Write(name);
            packet.Write(bullet);

            SendTCPDataToAllInRoomInLevel(roomId, packet);
        }
    }

    public static void PlayerDie(int roomId, int id, float respawnTime)
    {
        using (Packet packet = new Packet((int)ServerPackets.playerDie))
        {
            packet.Write(id);
            packet.Write(respawnTime);

            SendTCPDataToAllInRoomInLevel(roomId, packet);
        }
    }

    public static void PlayerRespawn(int roomId, int id)
    {
        using (Packet packet = new Packet((int)ServerPackets.playerRespawn))
        {
            packet.Write(id);

            SendTCPDataToAllInRoomInLevel(roomId, packet);
        }
    }
    #endregion

    public static void SpawnExplosion(int roomId, int type, Vector3 position, Vector3 scale)
    {
        using (Packet packet = new Packet((int)ServerPackets.spawnExplosion))
        {
            packet.Write(type);
            packet.Write(position);
            packet.Write(scale);

            SendTCPDataToAllInRoomInLevel(roomId, packet);
        }
    }
}
