﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ServerHandle
{
    #region Connection
    public static void WelcomeReceived(int fromClient, Packet packet)
    {
        int clientIdCheck = packet.ReadInt();

        Debug.Log($"{Server.clients[fromClient].tcp.socket.Client.RemoteEndPoint} connected successfully and is now player {fromClient}.");
        if (fromClient != clientIdCheck)
        {
            Debug.Log($"(ID: {fromClient}) has assumed the wrong client ID ({clientIdCheck})!");
        }
    }
    #endregion

    #region Room
    public static void CreateRoom(int fromClient, Packet packet)
    {
        string username = packet.ReadString();
        string roomname = packet.ReadString();

        Server.clients[fromClient].CreateRoom(roomname, username);
    }

    public static void JoinRoom(int fromClient, Packet packet)
    {
        string username = packet.ReadString();
        string roomname = packet.ReadString();

        Server.clients[fromClient].JoinRoom(roomname, username);
    }

    public static void LeaveRoom(int fromClient, Packet packet)
    {
        Server.clients[fromClient].LeaveRoom();
    }

    public static void ChangeRoomSetting(int fromClient, Packet packet)
    {
        int addLevel = packet.ReadInt();

        Server.rooms[Server.clients[fromClient].roomId].ChangeNextLevel(addLevel);
    }

    public static void LoadLevel(int fromClient, Packet packet)
    {
        int roomId = Server.clients[fromClient].roomId;

        Server.rooms[roomId].StartLevel();
    }

    public static void GetLevelInfo(int fromClient, Packet packet)
    {
        Server.rooms[Server.clients[fromClient].roomId].GetLevelInfo(fromClient);
    }
    #endregion

    #region Player
    public static void PlayerMovementInput(int fromClient, Packet packet)
    {
        int inputLength = packet.ReadInt();
        float[] inputArr = new float[inputLength];

        for(int i = 0; i < inputLength; i++)
        {
            float input = packet.ReadFloat();
            inputArr[i] = input;
        }

        if(Server.clients[fromClient].player == null)
            return;

        Server.clients[fromClient].player.SetInput(inputArr);
    }

    public static void PlayerShoot(int fromClient, Packet packet)
    {
        Player player = Server.clients[fromClient].player;
        player.GetComponent<PlayerWeaponController>().Shoot(player.roomId, player.facingDirection); ;
    }

    public static void PlayerSwitchWeapon(int fromClient, Packet packet)
    {
        bool nextWeapon = packet.ReadBool();

        Server.clients[fromClient].player.GetComponent<PlayerWeaponController>().SwitchNextWeapon(nextWeapon);
    }
    #endregion

    #region Obj
    public static void GetObjPosition(int fromClient, Packet packet)
    {
        string type = packet.ReadString();
        int id = packet.ReadInt();

        ServerSend.UpdateObjPosition(Server.clients[fromClient].roomId, type, id);
    }
    #endregion
}
