﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawnerController : MonoBehaviour
{
    public EnemySpawner[] enemySpawnerArr;
    public int[] enemyStartAmount;
    public int[] addEnemyEachWave;
    int[] enemyAmount;

    public List<Enemy> currentEnemyArr = new List<Enemy>();

    void Start()
    {
        foreach(EnemySpawner i in enemySpawnerArr)
        {
            i.GetComponent<RoomId>().id = GetComponent<RoomId>().id;
        }

        enemyAmount = new int[enemyStartAmount.Length];
        UpdateDiffculty();
    }

    void Update()
    {
        if (currentEnemyArr.Count <= 0)
        {
            SpawnEnemy();
            Server.rooms[GetComponent<RoomId>().id].wave++;
            UpdateDiffculty();
            ServerSend.InLevelMsg(GetComponent<RoomId>().id, "Wave " + Server.rooms[GetComponent<RoomId>().id].wave);
        }
    }


    public void SpawnEnemy()
    {
        for (int i = 0; i < enemySpawnerArr.Length; i++)
        {
            for (int j = 0; j < enemyAmount[i]; j++)
            {
                currentEnemyArr.Add(enemySpawnerArr[i].SpawnEnemy(this));
            }
        }
    }

    public void UpdateDiffculty()
    {
        for (int i = 0; i < enemyAmount.Length; i++)
        {
            enemyAmount[i] = enemyStartAmount[i] + Server.rooms[GetComponent<RoomId>().id].wave * addEnemyEachWave[i];
        }
    }
}
