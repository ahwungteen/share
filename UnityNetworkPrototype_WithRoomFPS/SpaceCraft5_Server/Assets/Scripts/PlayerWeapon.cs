﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerWeapon : MonoBehaviour
{
    [HideInInspector] public int type;
    public string weaponName;
    public float force = 300;
    public int damage = 10;
    public float destroyAfterDistence = 1;
    public float attackSpeed = 0.2f;
    public bool infinity;
    public int bulletAmount = 10;
    public int addBulletAmountEachTime;
    public float bulletSize = 0.1f;

    [Header("Explosion")]
    public bool explosion;
    public bool destroyByTime;
    public float explosionAfterTime;
    public float radius;
    public float explosionForce;

    public void Initialize(int _type)
    {
        type = _type;
    }

    public bool Shoot(int roomId, Vector3 position, Vector2 direction) // return false if no bullet
    {
        Projectile projectile = NetworkManager.instance.InstantiateObj("Projectile", roomId, position + (Vector3)direction * bulletSize / 2, 0).GetComponent<Projectile>();

        projectile.transform.localScale = new Vector3(bulletSize, bulletSize, 1);

        int projectileId = Server.projectiles.Count + 1;
        Server.projectiles.Add(projectileId, projectile);
        Server.rooms[roomId].projectileList.Add(projectileId);

        projectile.Initialize(projectileId, type, force, direction, damage, destroyAfterDistence);
        if (explosion)
            projectile.InitializeExplosion(explosion, destroyByTime, explosionAfterTime, radius, explosionForce);

        ServerSend.SpawnObjToAllInRoom(roomId, "Projectile", projectileId, 0, projectile.transform.position, projectile.transform.rotation, projectile.transform.localScale);

        if (infinity)
            return true;

        bulletAmount--;
        if (bulletAmount <= 0)
            return false;

        return true;
    }

    public void FillBullet()
    {
        bulletAmount += addBulletAmountEachTime;
    }
}
