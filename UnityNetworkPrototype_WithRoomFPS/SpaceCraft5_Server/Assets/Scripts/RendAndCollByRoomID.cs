﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RendAndCollByRoomID : MonoBehaviour
{
    public static int rendererId = 1;
    public bool notCol;

    RoomId roomId;
    Renderer rend;
    Collider2D col;

    void Awake()
    {
        GetComponent<Collider2D>().enabled = false;
    }

    void Start()
    {


        roomId = GetComponent<RoomId>();
        rend = GetComponent<Renderer>();
        rend.enabled = false;

        if (!notCol)
        {
            col = GetComponent<Collider2D>();
            col.enabled = true;
        }

        RendAndCollByRoomID[] obj = FindObjectsOfType<RendAndCollByRoomID>();
        foreach (RendAndCollByRoomID i in obj)
        {
            if (i.GetComponent<RoomId>().id != roomId.id)
            {
                Physics2D.IgnoreCollision(i.GetComponent<Collider2D>(), GetComponent<Collider2D>());
                if (i.GetComponent<CompositeCollider2D>() != null)
                {
                    Physics2D.IgnoreCollision(i.GetComponent<CompositeCollider2D>(), GetComponent<Collider2D>());
                }
            }
        }
    }

    void Update()
    {
        if (roomId.id == rendererId)
        {
            rend.enabled = true;
        }
        if (roomId.id != rendererId)
        {
            rend.enabled = false;
        }
    }
}
