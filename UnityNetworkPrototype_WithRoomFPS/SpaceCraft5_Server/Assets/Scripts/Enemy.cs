﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public int id;
    public int health = 30;
    public float speed = 1;
    public float attackSpeed = 0.1f;
    public float attackSpeedCount = 0.1f;
    public int damage = 10;
    public float findDistance = 30;

    Transform target;
    EnemySpawnerController enemySpawnerController;

    public Rigidbody2D rb;

    [HideInInspector] public bool destroyed;

    public void Initialize(int _id, EnemySpawnerController _enemySpawnerController)
    {
        id = _id;
        enemySpawnerController = _enemySpawnerController;
    }

    void Start()
    {
        FindTarget();
    }

    void FixedUpdate()
    {
        if (target == null || target.GetComponent<Player>().died ||
            (Mathf.Abs(transform.position.x - target.position.x) + Mathf.Abs(transform.position.y - target.position.y)) > findDistance)
        {
            FindTarget();
            return;
        }

        Vector2 direction = (target.position - transform.position).normalized;
        rb.position += direction * speed * Time.deltaTime;
    }

    void Update()
    {
        if (attackSpeedCount <= attackSpeed)
        {
            attackSpeedCount += Time.deltaTime;
        }
    }

    void FindTarget()
    {
        float distance = 10000;
        int minDistancePlayer = 0;
        bool haveTarget = false;

        foreach (int i in Server.rooms[GetComponent<RoomId>().id].playerList)
        {
            if (Server.clients[i].player != null && !Server.clients[i].player.died)
            {
                float currentDistance = Mathf.Abs(transform.position.x - Server.clients[i].player.transform.position.x) + Mathf.Abs(transform.position.y - Server.clients[i].player.transform.position.y);

                if (currentDistance < distance)
                {
                    distance = currentDistance;
                    minDistancePlayer = i;
                    haveTarget = true;
                }
            }

        }

        if (!haveTarget || distance > findDistance)
            return;

        target = Server.clients[minDistancePlayer].player.transform;
    }

    public void TakeDamage(int damage)
    {
        health -= damage;

        if (health <= 0)
        {
            float random = Random.Range(-1f, 1f);
            if (random > 0.2f)
            {
                SpawnItem();
            }
            DestroyEnemy();
        }
    }

    void SpawnItem()
    {
        Item item = NetworkManager.instance.InstantiateObj("Item", GetComponent<RoomId>().id, transform.position, 0).GetComponent<Item>();

        int itemId = Server.items.Count + 1;
        Server.items.Add(itemId, item);
        Server.rooms[GetComponent<RoomId>().id].itemList.Add(itemId);
        item.Initialize(itemId);

        ServerSend.SpawnObjToAllInRoom(GetComponent<RoomId>().id, "Item", itemId, 0, item.transform.position, item.transform.rotation, item.transform.localScale);
    }

    public void DestroyEnemy()
    {
        if (destroyed)
            return;

        ServerSend.DestroyObj(GetComponent<RoomId>().id, "Enemy", id);
        Server.rooms[GetComponent<RoomId>().id].enemyList.Remove(id);
        enemySpawnerController.currentEnemyArr.Remove(this);
        Destroy(Server.enemys[id].gameObject);

        destroyed = true;
    }

    void OnCollisionStay2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Attack(other.gameObject);
        }
    }

    void Attack(GameObject other)
    {
        if (attackSpeedCount > attackSpeed)
        {
            if (other.GetComponent<Player>() == null || other.GetComponent<Player>().destroyed)
                   return;
            other.GetComponent<Player>().TakeDamage(damage);
            attackSpeedCount = 0;
        }
    }
}
