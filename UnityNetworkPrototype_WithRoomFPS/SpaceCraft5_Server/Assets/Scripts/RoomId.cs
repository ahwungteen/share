﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomId : MonoBehaviour
{
    public int id;

    public void ChangeRendererId(int newId)
    {
        RendAndCollByRoomID.rendererId = newId;
    }
}
