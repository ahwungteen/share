﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room
{
    public int id;
    public string name;

    public GameObject fieldObj;
    public List<int> playerList = new List<int>();
    public List<int> projectileList = new List<int>();
    public List<int> enemyList = new List<int>();
    public List<int> itemList = new List<int>();

    public int level;
    public int nextLevel;
    public int wave = 0;

    public Room(int roomId)
    {
        id = roomId;
    }

    public void Initialize(string _name)
    {
        name = _name;
        level = 0;
        nextLevel = level;
    }

    public void ChangeNextLevel(int addLevel)
    {
        if (addLevel != 1 && addLevel != -1 && addLevel != 0)
        {
            Debug.Log("Error___ChangeNextLevel " + "Number Too Big or Small");
            return;
        }

        do
        {
            nextLevel += addLevel;

            if (nextLevel >= NetworkManager.instance.FieldPrefabArr.Length)
            {
                nextLevel = 0;
            }
            if (nextLevel < 0)
            {
                nextLevel = NetworkManager.instance.FieldPrefabArr.Length - 1;
            }
        } while (nextLevel == level);

        ServerSend.UpdateRoomSetting(id, nextLevel);
    }

    public void StartLevel()
    {
        foreach (int i in playerList)
        {
            if (Server.clients[i].player != null)
            {
                Server.clients[i].player.DestroyPlayer();
            }

            Server.clients[i].inLevel = false;
        }
        ClearRoomObj();

        level = nextLevel;
        fieldObj = NetworkManager.instance.InstantiateObj("Field", id, new Vector3(0, 0, 0), level);

        ServerSend.LoadLevelSceneToAllInRoom(id, 1);

        nextLevel = level + 1;
        if (nextLevel >= NetworkManager.instance.FieldPrefabArr.Length)
        {
            nextLevel = NetworkManager.instance.FieldPrefabArr.Length;
        }
    }

    public void GameOver()
    {
        nextLevel = 0;
        wave = 0;
        StartLevel();
    }

    public void LeaveRoom(int fromClient)
    {
        playerList.Remove(fromClient);
        if (playerList.Count == 0)
        {
            name = null;
            level = 0;
            nextLevel = level;
            wave = 0;
            ClearRoomObj();
        }
    }

    public void ClearRoomObj()
    {
        Object.Destroy(fieldObj);
        for (int i = projectileList.Count - 1; i >= 0; i--)
        {
            if (Server.projectiles.ContainsKey(projectileList[i]))
            {
                Server.projectiles[projectileList[i]].DestroyProjectile();
            }
        }

        for (int i = enemyList.Count - 1; i >= 0; i--)
        {
            if (Server.enemys.ContainsKey(enemyList[i]))
            {
                Server.enemys[enemyList[i]].DestroyEnemy();
            }
        }

        for (int i = itemList.Count - 1; i >= 0; i--)
        {
            if (Server.items.ContainsKey(itemList[i]))
            {
                Server.items[itemList[i]].DestroyItem();
            }
        }
    }

    public void GetLevelInfo(int fromClient)
    {
        Server.clients[fromClient].inLevel = true;

        ServerSend.SpawnObj(fromClient, "Field", 0, level, fieldObj.transform.position, fieldObj.transform.rotation, fieldObj.transform.localScale);
        SpawnPlayer(fromClient);

        foreach (int i in projectileList)
        {
            if (Server.projectiles[i] != null)
            {
                ServerSend.SpawnObj(fromClient, "Projectile", i, 0, Server.projectiles[i].transform.position, Server.projectiles[i].transform.rotation, Server.projectiles[i].transform.localScale);
            }
        }

        foreach (int i in enemyList)
        {
            if (Server.enemys[i] != null)
            {
                ServerSend.SpawnObj(fromClient, "Enemy", i, 0, Server.enemys[i].transform.position, Server.enemys[i].transform.rotation, Server.enemys[i].transform.localScale);
            }
        }
    }

    public void SpawnPlayer(int clientId)
    {
        Player player = NetworkManager.instance.InstantiateObj("Player", id, new Vector3(0, -2, 0), 0).GetComponent<Player>();
        player.Initialize(clientId, Server.clients[clientId].name, id);
        Server.clients[clientId].player = player;

        foreach (int i in playerList)
        {
            if (Server.clients[i].id != clientId && Server.clients[i].player != null)// spawn all in room players to fromClient
            {
                Player otherPlayer = Server.clients[i].player;
                ServerSend.SpawnObj(clientId, "Player", i, 0, otherPlayer.transform.position, otherPlayer.transform.rotation, otherPlayer.transform.localScale);
                ServerSend.InitializePlayer(clientId, i, Server.clients[i].name, otherPlayer.speed, otherPlayer.maxHealth);
                ServerSend.UpdatePlayerHealthUI(id, i, Server.clients[i].player.health);
                Server.clients[i].player.GetComponent<PlayerWeaponController>().UpdateWeaponUI();
            }
        }

        ServerSend.SpawnObjToAllInRoom(id, "Player", clientId, 0, player.transform.position, player.transform.rotation, player.transform.localScale); // spawn fromClient to all in room players
        ServerSend.InitializePlayerToAllInRoom(id, clientId, Server.clients[clientId].name, player.speed, player.maxHealth);
    }
}


