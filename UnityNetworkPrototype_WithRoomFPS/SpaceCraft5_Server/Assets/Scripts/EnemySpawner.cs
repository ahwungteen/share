﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public float spawnOffsetX = 1;
    public float spawnOffsetY = 1;

    public Enemy SpawnEnemy(EnemySpawnerController enemySpawnerController)
    {
        Vector3 position = new Vector3(transform.position.x + Random.Range(-spawnOffsetX, spawnOffsetX), transform.position.y + Random.Range(-spawnOffsetY, spawnOffsetY), 0);
        Enemy enemy = NetworkManager.instance.InstantiateObj("Enemy", GetComponent<RoomId>().id, position, 0).GetComponent<Enemy>();

        int enemyId = Server.enemys.Count + 1;
        Server.enemys.Add(enemyId, enemy);
        Server.rooms[GetComponent<RoomId>().id].enemyList.Add(enemyId);
        enemy.Initialize(enemyId, enemySpawnerController);

        ServerSend.SpawnObjToAllInRoom(GetComponent<RoomId>().id, "Enemy", enemyId, 0, enemy.transform.position, enemy.transform.rotation, enemy.transform.localScale);
        return enemy;
    }
}
